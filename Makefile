#!/usr/bin/make -f

all: fasttree

fasttree: FastTree.c
	gcc -DOPENMP -fopenmp -O3 -finline-functions -funroll-loops -Wall -o $@ $< -lm
